#!/bin/zsh
#mp3=$(file --mime-type * | grep audio | awk -F ":" '{print $1}')
#img=$(file --mime-type * | grep image | awk -F ":" '{print $1}')
#arc=$(file --mime-type * | grep zip | awk -F ":" '{print $1}')
#docx=$(file --mime-type * | grep vnd | awk -F ":" '{print $1}')
#pdf=$(file --mime-type * | grep pdf | awk -F ":" '{print $1}')
wk=$(pwd)
echo "Rangement des fichiers"

mkdir -p Music 
mkdir -p Pictures 
mkdir -p Documents
mkdir -p Archives 

#for fichier in $(ls)
for fichier in $(ls)
do
  echo $fichier
   type=$(file --mime-type $fichier | awk 'BEGIN {FS="/"} {print $1}' | awk 'BEGIN {FS=": "} {print $2}')
   soustype=$(file --mime-type -b $fichier | awk 'BEGIN {FS="/"} {print $2}')
echo $type

case "$type" in
  "audio") 
	    echo "mes audios" ; mv $fichier Music/. ;;
  "image") 
	    echo "mes images" ;mv $fichier Pictures/. ;;
  "application")
	   echo $fichier; 
		case "$soustype" in
			"pdf")
			      echo $fichier; mv $fichier Documents/. ;;
			vnd.*)
			      echo $fichier; mv $fichier Documents/. ;;
		        *zip)
			      echo $fichier; mv $fichier Archives/. ;; 	
		    
		esac ;;	
   #*) 
	#echo "$fichier : Inclassable" 
     
esac

done
