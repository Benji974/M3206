#!/bin/bash
HISTFILE=~/.bash_history
set -o history
history | awk '{print $2}' | sort | uniq -c | sort -nr | head -$1

#Tout d'abord il faut charger le fichier history afin que la commande fonctionne,ensuite on l'active avec set -o history
#Puis on lance la commande history, avec awk on affiche que la colonne contenant le nom des commandes, on les trie par ordre alphabetique, on les affiches de maniere unique avec uniq -c, puis on les tri a nouveau de maniere numerique du plus petit au plus grand, enfin avec head $1 on affiche que les X premiers resultats.
